import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAIEnemy;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class TrapSpots {

	public static Point addP(Point p1, Point p2){
		return new Point(p1.x+p2.x,p1.y+p2.y);
	}

	public static Point multP(Point p1, int n){
		return new Point(p1.x*n,p1.y*n);
	}

	public static ArrayList<Point> findTrapSpots(ApoSkunkmanAILevel level, Point pDef,int bombLen){

		ArrayList<Point> spots = new ArrayList<Point>();
		ArrayList<Point> listP = new ArrayList<Point>();

		// unten
		if (Helpers.passable(level, pDef.y + 1, pDef.x)	&& !Helpers.passable(level, pDef.y - 1, pDef.x) && !Helpers.passable(level, pDef.y, pDef.x - 1)	&& !Helpers.passable(level, pDef.y, pDef.x + 1)){
			listP.add(new Point(0,1));
		}

		// oben
		if (Helpers.passable(level, pDef.y - 1, pDef.x)	&& !Helpers.passable(level, pDef.y + 1, pDef.x) && !Helpers.passable(level, pDef.y, pDef.x - 1)	&& !Helpers.passable(level, pDef.y, pDef.x + 1))
			listP.add(new Point(0,-1));	

		// rechts
		if (Helpers.passable(level, pDef.y, pDef.x + 1)	&& !Helpers.passable(level, pDef.y, pDef.x - 1) && !Helpers.passable(level, pDef.y+1 , pDef.x )	&& !Helpers.passable(level, pDef.y-1, pDef.x))
			listP.add(new Point(1,0));

		// links
		if (Helpers.passable(level, pDef.y, pDef.x - 1)	&& !Helpers.passable(level, pDef.y, pDef.x + 1) && !Helpers.passable(level, pDef.y+1, pDef.x)	&& !Helpers.passable(level, pDef.y+1, pDef.x))
			listP.add(new Point(-1,0));

		while(!listP.isEmpty()){ // �berfl�ssig weil nur max 1 Element.
			Point pCheck = listP.remove(0);

			boolean vertical = pCheck.y == 0;

			for (int i = 1; i <= bombLen; i++) {
				Point tmp = addP(pDef,multP(pCheck,i)); // Punkte addieren und multiplizieren
				if (Helpers.pointWithinLevel(level, tmp) && Helpers.passable(level,tmp)) {
					spots.add(new Point(tmp));
					if (!vertical) {
						if (Helpers.passable(level, pDef.y, pDef.x + 1)	|| Helpers.passable(level, pDef.y, pDef.x - 1))
							break;
					} else {
						if (Helpers.passable(level, pDef.y + 1, pDef.x)	|| Helpers.passable(level, pDef.y - 1, pDef.x))
							break;
					}

				} else
					break;
			}
		}
		return spots;
	}

	public static int[][] chooseTrapSpots(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player, ArrayList<Point> listP){
		int[][] danger = new int[level.getLevelAsByte().length][level.getLevelAsByte()[0].length];

		final int tolerance = 0; // im wievielen z�gen wird der scheiss auftreten

		for (ApoSkunkmanAIEnemy enemy : level.getEnemies()) {
			if(enemy.isVisible()){
				int bombLength = enemy.getSkunkWidth();

				for (Point p : listP) {
					if(bombLength + 1 >= Helpers.pointDistanceInt(p, Helpers.getPlayerPoint(player))){ // evt ohne +1
						int timePlayer = Path.findPathToPoint(level, player, p,Helpers.getPlayerPoint(player)).getPassedTime();

						float startX = enemy.getX();
						float startY = enemy.getY();

						if(enemy.isMoving()){
							if(enemy.getVelocityX() != 0){
								if(enemy.getVelocityX() > 0 ){
									// aufrunden
									startX = (float) Math.ceil(startX); 
								}
								else{
									// abrunden
									startX = (float) Math.floor(startX);
								}
							}
							if(enemy.getVelocityY() != 0){
								if(enemy.getVelocityY() > 0 ){
									// aufrunden
									startY = (float) Math.ceil(startY);
								}
								else{
									// abrunden
									startY = (float) Math.floor(startY);
								}
							}
						}
						Point startPoint = new Point((int)startX,(int) startY);
						PathTile destEn = Path.findPathToPoint(level, enemy, p,startPoint);
						int timeEnemy = destEn.passedTime;
						if(destEn.passed){


							if(enemy.getMSUntilNextTile() != -1) timeEnemy += enemy.getMSUntilNextTile(); // zus�tzlich zeit wenn er sich bewegt

							if(timeEnemy <= timePlayer + tolerance ){
								for(int i = 0; i <= bombLength;i++){
									int y_ = p.y + (int)(i*(player.getPlayerY() -p.y)/(double)Math.abs(player.getPlayerY() - p.y));
									int x_ = p.x + (int)(i*(player.getPlayerX() -p.x)/(double)Math.abs(player.getPlayerX() - p.x));
									if(y_ < danger.length && x_ < danger[0].length && y_ >= 0 && x_ >= 0){
										if(danger[y_][x_] == 0 || danger[y_][x_] > timeEnemy)
											danger[y_][x_] = timeEnemy;
									}
								}
							}
						}
					}
				}
			}
		}

		return danger;
	}
	
	
	public static PathTileX trapEnemies(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player,BombSpots bombs,int[][] traps){
		for (ApoSkunkmanAIEnemy enemy : level.getEnemies()) {
			if(enemy.isVisible()){
				int bombLength =player.getSkunkWidth();
				
				float startX = enemy.getX();
				float startY = enemy.getY();

				if(enemy.isMoving()){
					if(enemy.getVelocityX() != 0){
						if(enemy.getVelocityX() > 0 ){
							// aufrunden
							startX = (float) Math.ceil(startX); 
						}
						else{
							// abrunden
							startX = (float) Math.floor(startX);
						}
					}
					if(enemy.getVelocityY() != 0){
						if(enemy.getVelocityY() > 0 ){
							// aufrunden
							startY = (float) Math.ceil(startY);
						}
						else{
							// abrunden
							startY = (float) Math.floor(startY);
						}
					}
				}
			
				ArrayList<Point> listP = findTrapSpots(level, new Point((int)startX,(int)startY), bombLength);
				
				for (Point pp : listP) {
					player.drawCircle(pp.x + 0.5f,pp.y+0.5f, 0.3f, false, 100,new Color(100, 100, 100));
				}
				
				for (Point p : listP) {
					if(bombLength + 1 >= Helpers.pointDistanceInt(p, Helpers.getPlayerPoint(player))){ // evt ohne +1
						PathTileX pathPl = PathX.findPatchToX(level, player, bombs, traps, p);

						Point startPoint = new Point((int)startX,(int)startY);
						PathTile destEn = Path.findPathToPoint(level, enemy, p,startPoint);
						int timeEnemy = destEn.passedTime;
						if(enemy.getMSUntilNextTile() != -1) timeEnemy += enemy.getMSUntilNextTile(); // zus�tzlich zeit wenn er sich bewegt
						
						


						if(pathPl.passed && pathPl.passedTime <= timeEnemy){
							if(pathPl.passedTime <= 40)
								pathPl.dropBomb = true;
							return pathPl;
			
						}
					}
				}
			}
		}

		return null;
	}
}
