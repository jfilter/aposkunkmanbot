import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAILevel;

public class BombSpots{
	
	public TreeSet<Integer>[][] time;
	
	// Clone
	@SuppressWarnings("unchecked")
	public BombSpots(BombSpots b,int tolerance){
		time = new TreeSet[b.time.length][b.time[0].length];
		
		for(int i = 0; i < b.time.length;i++){
			for(int j = 0; j < b.time[0].length; j++ ){
				time[i][j] = new TreeSet<Integer>();
				for (Integer in : b.time[i][j]) {
					if(in > tolerance)
					this.time[i][j].add(in - tolerance);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public BombSpots(ApoSkunkmanAILevel level,Skunk optional) {
		int yLength = level.getLevelAsByte().length;
		int xLength = level.getLevelAsByte()[0].length;
		byte[][] byteLevel = level.getLevelAsByte();
		
		time = new TreeSet[yLength][xLength];
		
		for(int i = 0; i < yLength; i++ ){
			for(int j = 0; j < xLength; j++)
				time[i][j] = new TreeSet<Integer>();
		}
		// Bedrohung ermitteln
		
		ArrayList<Skunk> explodeSkunk = new ArrayList<Skunk>();
		
		for (int i = 0; i < yLength; i++) {
			for (int j = 0; j < xLength; j++) {
				if (byteLevel[i][j] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN) {
					explodeSkunk.add(new Skunk(i,j,level.getSkunkman(i, j).getTimeToExplosion(),level.getSkunkman(i, j).getSkunkWidth())); //Fehler wo?
		}	}	}
		
		if(optional != null) explodeSkunk.add(optional);
		
		
		// Sortieren nach ersten Explosionen
		Collections.sort(explodeSkunk, new Comparator<Skunk>(){	public int compare(Skunk arg0, Skunk arg1) {return arg0.timeToExplode - arg1.timeToExplode;}});
		
		// Simmulieren	
		// Wenn die Bombe sp�ter explodiert muss die Zeit angepasst werden.
		for(int i = 0; i < explodeSkunk.size();i++){
			Skunk checkSkunk = explodeSkunk.get(i);
			if(!time[checkSkunk.y][checkSkunk.x].isEmpty() && time[checkSkunk.y][checkSkunk.x].first() < checkSkunk.timeToExplode) 
				checkSkunk.timeToExplode = time[checkSkunk.y][checkSkunk.x].first();
		
			for(int ii = checkSkunk.y;ii < yLength && ii <= checkSkunk.y + checkSkunk.radiusExplode;ii++){
				if(byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_FREE || byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_GOODIE || byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN )
					time[ii][checkSkunk.x].add(checkSkunk.timeToExplode);
				else{
					if(byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_BUSH)
						time[ii][checkSkunk.x].add(checkSkunk.timeToExplode);
					break; // keine W�nde durschlagen
			}	}
			for(int ii = checkSkunk.y;ii >= 0 && ii >= checkSkunk.y - checkSkunk.radiusExplode;ii--){
				if(byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_FREE || byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_GOODIE || byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN )
					time[ii][checkSkunk.x].add(checkSkunk.timeToExplode);
				else{
					if(byteLevel[ii][checkSkunk.x] == ApoSkunkmanAIConstants.LEVEL_BUSH)
						time[ii][checkSkunk.x].add(checkSkunk.timeToExplode);
					break; // keine W�nde durschlagen
			}	}
			for(int jj = checkSkunk.x; jj < xLength && jj <= checkSkunk.x + checkSkunk.radiusExplode;jj++){
				if(byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_FREE || byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_GOODIE || byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN )
					time[checkSkunk.y][jj].add(checkSkunk.timeToExplode);
				else{
					if(byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_BUSH)
						time[checkSkunk.y][jj].add(checkSkunk.timeToExplode);
					break; // keine W�nde durschlagen
			}	}
			for(int jj = checkSkunk.x; jj >= 0 && jj >= checkSkunk.x - checkSkunk.radiusExplode;jj--){
				if(byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_FREE || byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_GOODIE || byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN )
					time[checkSkunk.y][jj].add(checkSkunk.timeToExplode);
				else{
					if(byteLevel[checkSkunk.y][jj] == ApoSkunkmanAIConstants.LEVEL_BUSH)
						time[checkSkunk.y][jj].add(checkSkunk.timeToExplode);
					break; // keine W�nde durschlagen
}	}	}	}	}

class Skunk{
	int y, x, timeToExplode, radiusExplode;
	Skunk(int y, int x, int timeToExplode,int radiusExplode){
		this.y = y;
		this.x = x;
		this.timeToExplode = timeToExplode;
		this.radiusExplode = radiusExplode;
}	}