import java.awt.Point;

import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

// kurze Funktionen
public class Helpers {
	
	public static int roundTo20(int a) {
		return a + ApoSkunkmanAIConstants.MAX_WAIT_TIME - a % ApoSkunkmanAIConstants.MAX_WAIT_TIME;
	} // Auf 20er runden
	
	public static Point getPlayerPoint(ApoSkunkmanAIPlayer player){
		return new Point(player.getPlayerX(), player.getPlayerY());
	}
	
	public static boolean passable(ApoSkunkmanAILevel level, Point p){
		return passable(level, p.y, p.x);
	}
	
	public static boolean passable(ApoSkunkmanAILevel level, int y, int x) {
		return level.getLevelAsByte()[y][x] == ApoSkunkmanAIConstants.LEVEL_FREE || level.getLevelAsByte()[y][x] == ApoSkunkmanAIConstants.LEVEL_GOODIE;
	}
	
	public static boolean pointWithinLevel(ApoSkunkmanAILevel level, Point p){
		return level.getLevelAsByte().length > p.x && p.x > 0 && level.getLevelAsByte()[0].length > p.y && p.y > 0;
	}
	
	public static double pointDistance(Point p1, Point p2){
		return Math.sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y));
	}
	
	public static int pointDistanceInt(Point p1, Point p2){
		return (int) pointDistance(p1, p2);
	}
}
