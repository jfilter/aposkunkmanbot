import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeSet;

import apoSkunkman.ApoSkunkmanConstants;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class PathX {

	public static PathTileX findPatchToX(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player, BombSpots bombs, int[][] traps,Point destination){ 
		// f�r Treeset closed
		Comparator<Point> cp = new Comparator<Point>() {
			@Override
			public int compare(Point o1, Point o2) {
				if(o1.x == o2.x) return o1.y - o2.y;
				return o1.x - o2.x;
			}
		};

		ArrayList<PathTileX> open = new ArrayList<PathTileX>();
		TreeSet<Point> closed = new TreeSet<Point>(cp); // da "warten" an letzer Stelle gepr�ft wird, kann man sicher sein dass der Knoten nicht mehr ben�tigt wird, deswegen wird geschlossen.
		PathTileX curPathTile;

		boolean passed = false;
		Comparator<PathTileX> c = new Comparator<PathTileX>() {
			public int compare(PathTileX o1, PathTileX o2) {
			return o1.absTime+o1.passedTime - o2.absTime-o2.passedTime;

			}};

				open.add(new PathTileX(Helpers.getPlayerPoint(player))); // Startpunkt hinzuf�gen
				do {
					Collections.sort(open,c);
					curPathTile = open.get(0);
					open.remove(curPathTile);
					int curY = curPathTile.getPoint().y;
					int curX = curPathTile.getPoint().x;
					passed = destination.equals(curPathTile.p);
					if (!passed) {
						int movingTime = Helpers.roundTo20(player.getMSForOneTile());
						int holdingTime = ApoSkunkmanAIConstants.MAX_WAIT_TIME; 
						
						// mehr Zufall
						new PathTileX(curPathTile, curY, curX,holdingTime).addHold(level, open, closed, bombs,traps, player);
						new PathTileX(curPathTile, curY + 1, curX,movingTime).addToOpenX(level, open, closed, bombs,traps, player);
						new PathTileX(curPathTile, curY - 1, curX,movingTime).addToOpenX(level, open, closed, bombs,traps, player);
						new PathTileX(curPathTile, curY, curX + 1,movingTime).addToOpenX(level, open, closed, bombs,traps, player);
						new PathTileX(curPathTile, curY, curX - 1,movingTime).addToOpenX(level, open, closed, bombs,traps, player);
					}
				} while (!passed && !open.isEmpty());
				curPathTile.passed = passed;
				return curPathTile;
	}
}