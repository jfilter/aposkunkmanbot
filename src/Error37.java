import java.awt.Point;

import java.util.ArrayList;
import apoSkunkman.ApoSkunkmanConstants;
import apoSkunkman.ai.ApoSkunkmanAI;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAIEnemy;

import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class Error37 extends ApoSkunkmanAI {

	@Override
	public String getPlayerName() {
		return "error37";
	}

	@Override
	public String getAuthor() {
		return "Filter";
	}


	@Override
	public void think(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player) {
		try{
			long zstVorher, zstNachher;
			zstVorher = System.currentTimeMillis();
			String textForConsole = "";

			// Bomben lokalisieren
			BombSpots bombs = new BombSpots(level,null);

			// Alle Spots finden
			ArrayList<Point> allTraps = TrapSpots.findTrapSpots(level, Helpers.getPlayerPoint(player), ApoSkunkmanAIConstants.GAME_HEIGHT > ApoSkunkmanAIConstants.GAME_WIDTH ? ApoSkunkmanAIConstants.GAME_HEIGHT : ApoSkunkmanAIConstants.GAME_WIDTH );
			int[][] traps = TrapSpots.chooseTrapSpots(level, player, allTraps);

			for (Point pp : allTraps) {
				player.drawCircle(pp.x + 0.5f,pp.y+0.5f, 0.2f, false, 100);
			}
			// DRAW Chosen TRAPS
			int drawy = 0;
			int drawx = 0;	
			for (int[] is : traps) {

				for (int i : is) {
					if(i != 0)
						player.drawCircle(drawx, drawy, 0.1f, true, 1000);
					drawx++;
				}
				drawx = 0;
				drawy++;
			}
			// DRAW END

			// Wegfindung
			PathTile destination = null;


			if(Primary.inDanger(bombs,traps, player, level)){
				textForConsole += ("Gefahr!");
				destination = Path.findPatchToSafetyTile(level, player, bombs,traps,1);
				if(destination != null) destination.toDirection(level,player,traps,bombs);

			}
			else{
				PathTile pathTrap = TrapSpots.trapEnemies(level, player,bombs,traps);
				if(pathTrap != null){
					pathTrap.toDirection(level,player,traps,bombs);
					textForConsole += "Trapping :)";
				}
				else{

					if(level.getType() == ApoSkunkmanAIConstants.LEVEL_TYPE_GOAL_X){
						textForConsole += ("GoalX");
						PathTileX destX = PathX.findPatchToX(level, player, bombs, traps,level.getGoalXPoint());

						// DRAW PATHX
						PathTileX tmp = destX.pre;
						while(tmp.pre != null){
							player.drawLine(tmp.p.x + 0.5f, tmp.p.y+ 0.5f, tmp.pre.p.x+ 0.5f, tmp.pre.p.y+ 0.5f, 100);
							if(tmp.layBombBeforeThis) player.drawRect(tmp.p.x + 0.25f, tmp.p.y + 0.25f, 0.5f, 0.5f, false, 100);
							tmp = tmp.pre;
						}
						// DRAW END

						if(destX != null) destX.toDirection(level,player,traps,allTraps,bombs);			

					}
					if(level.getType() == ApoSkunkmanAIConstants.LEVEL_TYPE_STANDARD){
						textForConsole += ("Versus");

						Point pPl = new Point(player.getPlayerX(),player.getPlayerY());

						if(level.getTime() < ApoSkunkmanConstants.SKUNKMAN_TIME_TO_EXPLODE*level.getLevelAsByte().length){
							textForConsole += "Escaping Rocks";
							Point pMid =  Versus.getPointInTheMiddle(level);
							if(!Helpers.getPlayerPoint(player).equals(pMid)){
								PathTileX destX = PathX.findPatchToX(level, player, bombs, traps, pMid);
								destX.toDirection(level, player, traps, allTraps, bombs);
							}
						}


						if(Versus.enemyInRange(level, player) && Primary.canLay(level, player, traps, bombs)){
							player.laySkunkman();
						}
						else{
							// N�hesten gegner suchen

							Point pEn = null;
							int min = Integer.MAX_VALUE;

							for (ApoSkunkmanAIEnemy enemy : level.getEnemies()) {
								if(enemy.isVisible()){
									Point pE = new Point((int)enemy.getX(), (int)enemy.getY());
									if(Helpers.pointDistance(pPl, pE) < min){
										min = (int) Helpers.pointDistance(pPl, pE);
										pEn = pE;
									}
								}
							}
							try{
								PathTileX pathEn = PathX.findPatchToX(level, player, bombs, traps,pEn);
								
								if(Math.random() > 0.05) // Der Bot setzt sehr selten man aus um den LeftRightBot zu schlagen
									pathEn.toDirection(level, player, traps, allTraps, bombs);

								// DRAW PATHX
								PathTileX tmp = pathEn.pre;
								while(tmp.pre != null){
									player.drawLine(tmp.p.x + 0.5f, tmp.p.y+ 0.5f, tmp.pre.p.x+ 0.5f, tmp.pre.p.y+ 0.5f, 100);
									if(tmp.layBombBeforeThis) player.drawRect(tmp.p.x + 0.25f, tmp.p.y + 0.25f, 0.5f, 0.5f, false, 100);
									tmp = tmp.pre;
								}
								// DRAW END

							}catch (Exception e) {
								System.out.println("nullpointer gotoenemy");
							}
							textForConsole += " Go to nearest Enemy ";
						}
					}
				}
			}
			if(level.getType() == ApoSkunkmanAIConstants.LEVEL_TYPE_GOAL_X && 
					Primary.forceWin(level, player)) textForConsole += " Forced";

			zstNachher = System.currentTimeMillis();
			textForConsole += (" in: " + (zstNachher - zstVorher) + "MS");
			player.addMessage(textForConsole);
		}catch (Exception e) {
			System.out.println(":(");
		}
	}
}