import java.awt.Point;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAIEnemy;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class Versus {
	public static Point getPointInTheMiddle(ApoSkunkmanAILevel level){
		byte[][] levelArray = level.getLevelAsByte();

		int half = levelArray.length/2;

		if(levelArray[half][half] != ApoSkunkmanAIConstants.LEVEL_STONE){
			return new Point(half, half);
		}
		if(levelArray[half+1][half] != ApoSkunkmanAIConstants.LEVEL_STONE){
			return new Point(half+1, half);
		}
		if(levelArray[half][half+1] != ApoSkunkmanAIConstants.LEVEL_STONE){
			return new Point(half, half+1);
		}
		if(levelArray[half-1][half] != ApoSkunkmanAIConstants.LEVEL_STONE){
			return new Point(half-1, half);
		}
		if(levelArray[half][half-1] != ApoSkunkmanAIConstants.LEVEL_STONE){
			return new Point(half, half-1);
		}

		double rnd = 0;

		// Wenn nicht die offentsichlichtes dann halt random :)
		while(true){
			int y = half + (int)(rnd*Math.random());
			int x = half + (int)(rnd*Math.random());
			if(levelArray[y][x] != ApoSkunkmanAIConstants.LEVEL_STONE){
				return new Point(x, y);
			}
			rnd += 0.1*Math.random();
		}
	}

	public static boolean enemyInRange(ApoSkunkmanAILevel level,ApoSkunkmanAIPlayer player){

		for (ApoSkunkmanAIEnemy enemy : level.getEnemies()) {
			if(enemy.isVisible()){
				float startX = enemy.getX();
				float startY = enemy.getY();

				if(enemy.isMoving()){
					if(enemy.getVelocityX() != 0){
						if(enemy.getVelocityX() > 0 ){
							// aufrunden
							startX = (float) Math.ceil(startX); 
						}
						else{
							// abrunden
							startX = (float) Math.floor(startX);
						}
					}
					if(enemy.getVelocityY() != 0){
						if(enemy.getVelocityY() > 0 ){
							// aufrunden
							startY = (float) Math.ceil(startY);
						}
						else{
							// abrunden
							startY = (float) Math.floor(startY);
						}
					}
				}
				
				Point pEn = new Point((int)(startX), (int)(startY));
				Point pPl = Helpers.getPlayerPoint(player);
		
				double dist = Helpers.pointDistance(pEn,pPl);
				
				
				boolean passed = true;
				if(dist <= player.getSkunkWidth()  && dist % 1d == 0d){
					int x = pEn.x - pPl.x;
					int y = pEn.y - pPl.y;


					for(int i = 1; i <= dist; i++){
						if(y == 0){
							if(level.getLevelAsByte()[pPl.y][pPl.x + i*Math.abs(x)/x] == ApoSkunkmanAIConstants.LEVEL_FREE
									|| level.getLevelAsByte()[pPl.y][pPl.x + i*Math.abs(x)/x] == ApoSkunkmanAIConstants.LEVEL_GOODIE)
								;
							else
								passed = false;
						}
						else
						{
							if(x == 0){
								if(level.getLevelAsByte()[pPl.y  + i*Math.abs(y)/y][pPl.x] == ApoSkunkmanAIConstants.LEVEL_FREE
										|| level.getLevelAsByte()[pPl.y  + i*Math.abs(y)/y][pPl.x] == ApoSkunkmanAIConstants.LEVEL_GOODIE)
									;
								else
									passed = false;
							}
						}
					}
					if(passed)
						return true;
				}
			}
		}
		return false;
	}
}
