import java.awt.Point;
import java.util.ArrayList;
import java.util.TreeSet;

import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAIEnemy;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class PathTile {

	private PathTile pre; // vorg�nger
	public int passedTime, actionTime, threatLevel, threatSingle;
	boolean dropBomb = false;
	Point p = new Point();
	boolean passed;

	// weiteren Punkte
	public PathTile(PathTile pre, int y, int x, int actionTime){
		this.pre = pre;
		this.actionTime = actionTime;		
		this.passedTime = pre.getPassedTime() + actionTime;
		this.p.y = y;
		this.p.x = x;
		this.threatLevel = pre.getThreatLevel();
	}
	// Startpunkt
	public PathTile(Point p){
		this.p = p;
		this.passedTime = 0;
		this.actionTime = 0;
		this.threatLevel = 0;
		this.pre = null;
	}

	public void changeThreat(int akk)
	{
		threatSingle = akk;
		threatLevel += akk;
	}

	// neues Element wird hinzugef�gt wenn es noch nicht in der closed/open Liste ist.
	// wenn es in open ist wird veglichen und evt angepasst.
	// R�ckgabe gibt an, ob ein neues Element hinzhugef�gt wurde.
	
	public boolean addToList(ApoSkunkmanAILevel level,ArrayList<PathTile> open, ArrayList<PathTile> closed, BombSpots bombs, int[][] traps,ApoSkunkmanAIEnemy player){

		if(this.reachable(level, bombs.time[p.y][p.x],player.getMSForOneTile())){ // oder ob sie schon explodiert ist wenn man es erreicht
			for(int i = 0; i < closed.size();i++){
				PathTile tmpTile = closed.get(i);
				if(tmpTile.getPoint().equals(p)){ // zeit muss auch gleich sein?
					return false;
				}	}
			// calcThreat

			//			if(traps[p.y][p.x] < (passedTime)) changeThreat(1);
			if(traps[p.y][p.x] != 0) changeThreat(1);


			for(int i = 0; i < open.size() ;i++){
				PathTile tmpTile = open.get(i);
				if(tmpTile.getPoint().equals(p) && tmpTile.getPassedTime() == passedTime){ //zeit muss auch gleich sein?
					if(tmpTile.getThreatLevel() > threatLevel){
						open.remove(tmpTile);
						open.add(this);
					}
					return false;
				}	}
			open.add(this);
			return true;
		}
		return false;	
	}

	public  boolean addToListSimple(ApoSkunkmanAILevel level,ArrayList<PathTile> open, ArrayList<PathTile> closed,ApoSkunkmanAIEnemy player){
		if(Helpers.passable(level, p)){
			for(int i = 0; i < closed.size();i++){
				PathTile tmpTile = closed.get(i);
				if(tmpTile.getPoint().equals(p)){ // zeit muss auch gleich sein?
					return false;
				}	}

			for(int i = 0; i < open.size() ;i++){
				PathTile tmpTile = open.get(i);
				if(tmpTile.getPoint().equals(p)){ //zeit muss auch gleich sein?
					return false;
				}	}
			open.add(this);
			return true;
		}
		return false;	
	}

	// Gibt an ob die Position erreichbar ist oder nicht.
	private  boolean reachable(ApoSkunkmanAILevel level,TreeSet<Integer> explodeTime, int moveOneTileTime){
		byte lvlByte[][] = level.getLevelAsByte();
		if(Helpers.pointWithinLevel(level, p) && !(lvlByte[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_STONE)){
			// �berpr�fung ob Tile bis dahin frei ist
			if(!(lvlByte[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN || lvlByte[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_BUSH ) || (!explodeTime.isEmpty() &&  passedTime > explodeTime.first())) //implikation
				// �berpr�fung auf Explosionsgefahr. Entweder leer, oder Explosion hat schon statt gefunden, oder man hat noch die Chance von da ab zuhauen.
				return explodeTime.isEmpty() || explodeTime.higher(passedTime - actionTime - 1) == null || explodeTime.higher(passedTime - actionTime - 1) > passedTime  + moveOneTileTime + ApoSkunkmanAIConstants.MAX_WAIT_TIME; 
		}
		return false;
	}

	public void toDirection(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player,int[][] traps,BombSpots bombs) {
		// ein Schritt in Richtung durchf�hren oder warten
		if (dropBomb)
		{
			if(Primary.canLay(level, player, traps, bombs))
			player.laySkunkman();		

		}

		else {
			if (p.x != player.getPlayerX() || p.y != player.getPlayerY()) {
				if (pre.pre != null)
					pre.toDirection(level,player,traps,bombs);
				else {
					if (p.x - 1 == player.getPlayerX())
						player.movePlayerRight();
					if (p.x + 1 == player.getPlayerX())
						player.movePlayerLeft();
					if (p.y - 1 == player.getPlayerY())
						player.movePlayerDown();
					if (p.y + 1 == player.getPlayerY())
						player.movePlayerUp();
				}
			}
		}
	}

	public PathTile getPre() {
		return pre;
	}

	public int getPassedTime() {
		return passedTime;
	}

	public int getThreatLevel() {
		return threatLevel;
	}

	public void setThreatLevel(int threatLevel) {
		this.threatLevel = threatLevel;
	}

	public int getX(){
		return p.x;
	}

	public int getY(){
		return p.y;
	}

	public Point getPoint() {
		return p;
	}	}