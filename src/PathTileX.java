import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.TreeSet;

import apoSkunkman.ApoSkunkmanConstants;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAIEnemy;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

public class PathTileX extends PathTile {

	final int diff = 10*ApoSkunkmanConstants.SKUNKMAN_TIME_TO_EXPLODE;
	PathTileX pre;
	int absTime;
	boolean layBombBeforeThis = false;

	public PathTileX(Point p) {
		super(p);
		absTime = 0;
	}

	public PathTileX(PathTileX pre, int y, int x, int actionTime){
		super(pre,y,x,actionTime);
		this.pre = pre;
		absTime = pre.absTime;
	}

	public void addHold(ApoSkunkmanAILevel level,ArrayList<PathTileX> open, TreeSet<Point> closed, BombSpots bombs, int[][] traps,ApoSkunkmanAIPlayer player){

		int maxY = level.getLevelAsByte().length;
		int maxX = level.getLevelAsByte()[0].length;
		int tt = 0;

		if(p.x + 1 > 0 && p.x + 1 < maxX){
			if(!bombs.time[p.y][p.x+1].isEmpty()){
				tt = bombs.time[p.y][p.x+1].first();
			}
		}

		if(p.x - 1 > 0 && p.x - 1 < maxX){
			if(!bombs.time[p.y][p.x-1].isEmpty()){
				if(bombs.time[p.y][p.x-1].first() > tt) tt = bombs.time[p.y][p.x-1].first();
			}
		}

		if(p.y + 1 > 0 && p.y + 1 < maxY){
			if(!bombs.time[p.y+1][p.x].isEmpty()){
				if(bombs.time[p.y+1][p.x].first() > tt) tt = bombs.time[p.y+1][p.x].first();
			}
		}

		if(p.y - 1 > 0 && p.y - 1 < maxY){
			if(!bombs.time[p.y-1][p.x].isEmpty()){
				if(bombs.time[p.y-1][p.x].first() > tt) tt = bombs.time[p.y-1][p.x].first();
			}
		}

		if(tt != 0){
			tt = Helpers.roundTo20(tt);
			boolean passed = true;
			PathTileX tp = this;
			while(tt > 0){
				tp = new PathTileX(tp, tp.p.y, tp.p.x, 20);

				if(!tp.checkAdd(level, open, closed, bombs, traps, player)){
					passed = false;
					break;
				}

				tt-=20;
			}
			if(passed)
				open.add(tp);
			//			player.drawCircle(p.x+0.5f, p.y+0.5f, 0.2f, true, 21);
		}
		closed.add(p);
	}

	public void addToOpenX(ApoSkunkmanAILevel level,ArrayList<PathTileX> open, TreeSet<Point> closed, BombSpots bombs, int[][] traps,ApoSkunkmanAIPlayer player){
		if(checkAdd(level, open, closed, bombs, traps, player)) open.add(this);	
	}

	public boolean checkAdd(ApoSkunkmanAILevel level,ArrayList<PathTileX> open, TreeSet<Point> closed, BombSpots bombs, int[][] traps,ApoSkunkmanAIPlayer player){

		if(reachable(level, bombs.time[p.y][p.x],player)){ // oder ob sie schon explodiert ist wenn man es erreicht
			if(closed.contains(p)) return false;

			// calcThreat
			if(traps[p.y][p.x] > passedTime) return false;

			// wenn schon alles zb keine upgrades oder andersrum!

			// Goodie
			if(level.getLevelAsByte()[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_GOODIE)
				switch(level.getGoodie(p.y, p.x).getGoodie()){
				case ApoSkunkmanAIConstants.GOODIE_GOOD_GOD:
					absTime -= 20*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_GOOD_FAST:
					absTime -= 5*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_GOOD_SKUNKMAN:
					absTime -= 5*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_GOOD_WIDTH:
					absTime -= 5*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_BAD_GOD:
					absTime += 20*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_BAD_FAST:
					absTime += 5*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_BAD_SKUNKMAN:
					absTime += 5*diff; break;
				case ApoSkunkmanAIConstants.GOODIE_BAD_WIDTH:
					absTime += 5*diff; break;
				}

			for(int i = 0; i < open.size() ;i++){
				PathTileX tmpTile = open.get(i);
				if(tmpTile.getPoint().equals(p) && tmpTile.getPassedTime() == passedTime){ //zeit muss auch gleich sein?
					if(tmpTile.getThreatLevel() > threatLevel){
						open.remove(tmpTile);
						open.add(this);
					}
					return false;
				}	}

			int tolerance = 100;
			if(level.getLevelAsByte()[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_BUSH)
			{
				if(bombs.time[p.y][p.x].isEmpty() || passedTime-actionTime - tolerance <= bombs.time[p.y][p.x].first()){

					//				if(!bombs.time[p.y][p.x].isEmpty()) player.drawCircle(p.x +0.5f, p.y+0.5f, 0.1f, true, 100,new Color(0, 255, 0));

					layBombBeforeThis = true;
					absTime += diff; //rnd
				}
				else
				{
					return false;
				}
			}
			else{

			}
			return true;
		}
		return false;	
	}

	private  boolean reachable(ApoSkunkmanAILevel level,TreeSet<Integer> explodeTime,ApoSkunkmanAIPlayer pl){
		byte lvlByte[][] = level.getLevelAsByte();
		if(Helpers.pointWithinLevel(level, p) && !(lvlByte[p.y][p.x] == ApoSkunkmanAIConstants.LEVEL_STONE)){
			// �berpr�fung ob Tile bis dahin frei ist
			final int tolerance = 1;
			// �berpr�fung auf Explosionsgefahr. Entweder leer, oder Explosion hat schon statt gefunden, oder man hat noch die Chance von da ab zuhauen.
			return explodeTime.isEmpty() || explodeTime.higher(passedTime - actionTime - tolerance) == null || explodeTime.higher(passedTime - actionTime - tolerance) > passedTime  + ApoSkunkmanAIConstants.MAX_WAIT_TIME +2*Helpers.roundTo20(pl.getMSForOneTile());
		}
		return false;
	}

	public void toDirection(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player,int[][] traps,ArrayList<Point> allTraps,BombSpots bombs) {
		// ein Schritt in Richtung durchf�hren oder warten
		PathTileX cur = this;

		Point pBomb = new Point();
		Point pPlayer = new Point(player.getPlayerX(),player.getPlayerY());

		while(cur.pre.pre!= null){
			cur = cur.pre;
			if(cur.layBombBeforeThis){
				pBomb = cur.p;
			}
			// ob um eine Ecke genagen wurde
			if(pBomb != null && Helpers.pointDistance(pBomb, cur.p) % 1d != 0d)
				pBomb = null;
		}
		boolean go = false;
		// Bei ausreichender Bombenst�rke k�nnen die Bomben etwas weiter weg vom Bush gesetzt werden.
		if (pBomb != null && Helpers.pointDistance(pBomb, pPlayer) <= player.getSkunkWidth()
				&& Helpers.pointDistance(pBomb, pPlayer)%1d == 0d ) {
			if(Primary.canLay(level, player, traps,bombs)){
				// Damit er nicht immer "lange" Bomben nimmt.
				if(Helpers.passable(level, pPlayer.y, pPlayer.x + 1) || Helpers.passable(level, pPlayer.y, pPlayer.x - 1)){
					go = !Helpers.passable(level, pPlayer.y+ 1, pPlayer.x) &&  !Helpers.passable(level, pPlayer.y - 1,pPlayer.x);
				}
				if(go && (Helpers.passable(level, pPlayer.y - 1, pPlayer.x) || Helpers.passable(level, pPlayer.y + 1, pPlayer.x)))
					go=  !Helpers.passable(level, pPlayer.y, pPlayer.x + 1) && !Helpers.passable(level, pPlayer.y, pPlayer.x - 1);
			}
		}

		if(!go) go = (pBomb != null && Helpers.pointDistance(pBomb, pPlayer) == 1);
		if(go && Primary.canLay(level, player, traps,bombs)) player.laySkunkman();	
		else {
			if(allowedToGoThere(level, player, cur.p)){
				if (cur.p.x - 1 == player.getPlayerX())	player.movePlayerRight();
				if (cur.p.x + 1 == player.getPlayerX())	player.movePlayerLeft();
				if (cur.p.y - 1 == player.getPlayerY())	player.movePlayerDown();
				if (cur.p.y + 1 == player.getPlayerY())	player.movePlayerUp();
			}
		}
	}

	private static boolean allowedToGoThere(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player,Point p){
		boolean go  = false;
		int checkLength = 0;
		int plX = (int) player.getX();
		int plY = (int) player.getY();

		if(level.getLevelAsByte()[plY][plX] == ApoSkunkmanAIConstants.LEVEL_SKUNKMAN){
			go = true;
			checkLength = level.getSkunkman(plY, plX).getSkunkWidth();
		}

		if(!go){
			for (ApoSkunkmanAIEnemy enemy : level.getEnemies()) {
				if(enemy.isVisible() && enemy.getX() == p.x && enemy.getY() == p.y){
					go = true;
					checkLength = enemy.getSkunkWidth();
					break;
				}
			}
		}

		if(go) {
			Point goP = new Point(p);
			Point akkP = new Point();

			int x = player.getPlayerX() - p.x;
			int y = player.getPlayerY() - p.y;

			if(x < 0) akkP.x = 1;
			if(x > 0) akkP.x = -1;

			if(y < 0) akkP.y = 1;
			if(y > 0) akkP.y = -1;

			for(int i = 1; i <= checkLength;i++){
				
				if(calcPassables(level, goP) <= 1) return false;
				goP.x += akkP.x;
				goP.y += akkP.y;

			}
			return true;
		}
		else
			return true;
	}

	private static int calcPassables(ApoSkunkmanAILevel level ,Point p){
		int count = 0;
		Point pu = new Point(p);
		pu.y -= 1;
		Point pd = new Point(p);
		pd.y += 1;
		Point pl = new Point(p);
		pl.x -= 1;
		Point pr = new Point(p);
		pr.x += 1;

		if(Helpers.pointWithinLevel(level, pu) && Helpers.passable(level,pu)) 	count++;
		if(Helpers.pointWithinLevel(level, pd) && Helpers.passable(level,pd))	count++;
		if(Helpers.pointWithinLevel(level, pl) && Helpers.passable(level,pl))	count++;
		if(Helpers.pointWithinLevel(level, pr) && Helpers.passable(level,pr))	count++;


		System.out.println(count);
		return count;
	}
}