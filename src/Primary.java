import java.awt.Color;
import java.awt.Point;

import apoSkunkman.ApoSkunkmanConstants;
import apoSkunkman.ai.ApoSkunkmanAIConstants;
import apoSkunkman.ai.ApoSkunkmanAILevel;
import apoSkunkman.ai.ApoSkunkmanAIPlayer;

// zus�tzliche allgemeinere Funktionen
public class Primary {

	// �berpr�fung auf Gefahr
	public static boolean inDanger(BombSpots bombs,int[][] traps, ApoSkunkmanAIPlayer player,ApoSkunkmanAILevel level){
		
		int[][] traps_ = new int[traps.length][traps[0].length];
		// auf traps wird nicht weiter geachtet. nur bomben werden reduziert
		for (int[] is : traps) {
			for (int i : is) {
				if(i != 0) return true;
			}
		}
		if(!bombs.time[player.getPlayerY()][player.getPlayerX()].isEmpty())
			if(bombs.time[player.getPlayerY()][player.getPlayerX()].first() <= Helpers.roundTo20(player.getMSForOneTile())) return true;
		
		final int nextTime = ApoSkunkmanAIConstants.MAX_WAIT_TIME;	
		BombSpots bombs_ = new BombSpots(bombs,nextTime);
		PathTile check = Path.findPatchToSafetyTile(level, player, bombs_, traps_,0);
		return !check.passed;
	}
	
	// �berpr�fung, ob Bombe gelegt werden kann
	public static boolean canLay(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player,int[][] traps,BombSpots oldBombs){
		
		if(!player.canPlayerLayDownSkunkman()) return false;

		BombSpots bombs = new BombSpots(new BombSpots(level, new Skunk(player.getPlayerY(), player.getPlayerX(), ApoSkunkmanConstants.SKUNKMAN_TIME_TO_EXPLODE, player.getSkunkWidth())),ApoSkunkmanAIConstants.MAX_WAIT_TIME);
		
		return Path.findPatchToSafetyTile(level, player, bombs, traps, 0).passed;
	}
	
	// Wenn Spieler 1 Tile vom Ziel enfernt ist wird der Zug zum X erzwungen.(Bot macht manchmal Probleme)
	public static boolean forceWin(ApoSkunkmanAILevel level, ApoSkunkmanAIPlayer player){
		Point pPlayer = new Point(player.getPlayerX(),player.getPlayerY());
		Point pX = level.getGoalXPoint();
		if(Helpers.pointDistance(pPlayer,pX ) == 1d){
			if (pX.x - 1 == pPlayer.x)	player.movePlayerRight();
			if (pX.x + 1 == pPlayer.x)	player.movePlayerLeft();
			if (pX.y - 1 == pPlayer.y)	player.movePlayerDown();
			if (pX.y + 1 == pPlayer.y)	player.movePlayerUp();
			return true;
		}
		return false;
	}
}